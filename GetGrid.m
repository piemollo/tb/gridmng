%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [X,Y,Z] = GetGrid(grd, ngrd)
%
% This [private] function convert list description in meshgrid description.
% See GetList for the opposite conversion.
%
%  input: - grd: grid points list (array n1*n2 x 3)
%         - ngrd: grid dimension in each direction
%
% output: - X: x coordinates
%         - Y: y coordinates
%         - Z: z coordinates
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X,Y,Z] = GetGrid(grd, ngrd)
  X = reshape(grd(:,1), ngrd(1), ngrd(2));
  Y = reshape(grd(:,2), ngrd(1), ngrd(2));
  Z = reshape(grd(:,3), ngrd(1), ngrd(2));
end


