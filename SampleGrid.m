% ====== Section management pipeline
% Here with a custom section

% --- Section's normal + inner point
VN = [ 0, 0.73, 0.68];
Inner_point = [-3.77, 30.0137, -9.14646];
% --- Sub-grid bound boxes

% --- Pre-structured grid
Sect.plane = BuildSection(VN, Inner_point);
Sect.resolution = 1;
Sect.unit = 'mm';
Sect.boundbox = [-63.19, 53.15; -6.924, 91.54; -70.73, 71.00];
Sect.sbb1 = [-inf, -47; 25, 35; -17, 2];
Sect.sbb2 = [40, inf; 30, 40; -20, 0];

% --- Set up grid
[Sect.grid, Sect.ngrid] = SetGrid(Sect);
[Sect.subgrid{1}, Sect.nsubgrid{1}, Sect.subindex{1}] = SubGrid(Sect.grid, Sect.ngrid, Sect.sbb1);
[Sect.subgrid{2}, Sect.nsubgrid{2}, Sect.subindex{2}] = SubGrid(Sect.grid, Sect.ngrid, Sect.sbb2);


% --- Figure
figure(1)
clf
hold on

% Display grids
DispGrid(Sect.grid, "*b","markersize",2);
DispGrid(Sect.subgrid{1}, "*g","markersize",3);
DispGrid(Sect.subgrid{2}, "*c","markersize",3);

axis equal

% Overlay
title("Visualisation mesh section")
xlabel("x")
ylabel("y")
zlabel("z")

% --- Save grids
% SaveGrid([Sect.subgrid{1}; Sect.subgrid{2}], "./subsect_0001.dat");