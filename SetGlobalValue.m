%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [val] = SetGlobalValue(Sect, K, valmax)
%
% Work in progress
%
%  input: - Sect: cell containing grid metadata
%         - K: 
%         -valmax:
%
% output: - val: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [val] = SetGlobalValue(Sect, K, valmax)
  val = zeros(size(Sect.grid, 1),1);
  
  if exist('K','var')==0
    K = 1;
  end
  
  for i=1:length(Sect.subgrid)
    val(Sect.subindex{i}==1) = Sect.subeval{i}(:,K);
  end
  
  if exist('valmax','var')==0
    valmax = max(val);
  end
  val = val./valmax;
  
end