%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [] = DispGrid(grd, varargin)
%
% Display a grid with given options
%
%  input: - grd: grid to display
%         - varargin: display option (color, markersize, etc.)
%
% output: none
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = DispGrid(grd,varargin)
  plot3(grd(:,1), grd(:,2), grd(:,3), varargin{:});
end