%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [Z] = GetPoint(plane, X, Y)
%
% This function returns the last coordinate of plane-inside points for the
% given sets of first and second coordinates.
%
%  input: - plane: vector describing the pane
%         - X: set of first coordinate
%         - Y: set of second coordinate
%
% output: - Z: set of last coordinate
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Z] = GetPoint(plane, X, Y)
	Z = plane(1) .* X;
	Z = Z + plane(2) .* Y;
	Z = Z + plane(4) .* ones(size(X,1), size(X,2));
	Z = -Z .* (1/plane(3));
end
