%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [msh] = MeshifyGrid(Sect)
%
% This function converts section into conform shell mesh [WiP]
%
%  input: - Sect: cell containing grid metadata
%
% output: - msh: mesh structure
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [msh] = MeshifyGrid(Sect)
  % --- Elementary data
  msh.type  = 'surface';
  msh.Nodes = Sect.grid;
  msh.Elements = delaunayn([Sect.grid(:,1), Sect.grid(:,2)]);
  msh.GeometricOrder = 'quadratic';
  
  % --- Labelling
  % background label = 0
  msh.LabelNodes    = zeros(size(Sect.grid,1),1);
  msh.LabelElements = zeros(size(msh.Elements,1),1);
  
  % --- Define regions
  if isfield(Sect, "subgrid")
    for i=1:length(Sect.subgrid)
     msh.LabelNodes(Sect.subindex{i}==1) = i;
     id = prod(msh.LabelNodes(msh.Elements(:,1),:) == (i+1), 2);
     msh.LabelElements(id==1) = i; 
    end
  end
  
end